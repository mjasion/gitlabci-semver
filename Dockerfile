FROM node:12-alpine
RUN apk add git --no-cache
RUN npm install -g semantic-release @semantic-release/gitlab
RUN npm install -g semantic-release @semantic-release/exec
RUN npm install -g semantic-release conventional-changelog-conventionalcommits

